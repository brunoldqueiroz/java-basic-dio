import java.util.Scanner;

public class ContaTerminal {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Número da conta: ");
        int conta = sc.nextInt();
        sc.nextLine();

        System.out.print("Informe sua agência: ");
        String agencia = sc.nextLine();

        System.out.print("Informe seu nome: ");
        String nome = sc.nextLine();

        System.out.print("Qual o seu saldo? ");
        double saldo = sc.nextDouble();

        ContaCliente cliente = new ContaCliente(conta, agencia, nome, saldo);

        System.out.println();
        System.out.println(cliente);


        sc.close();
    }
}