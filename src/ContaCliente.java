public class ContaCliente {
    private int numero;
    private String nome;
    private String agencia;
    private double saldo;

    public ContaCliente(int numero, String agencia, String nome, double saldo) {
        this.numero = numero;
        this.agencia = agencia;
        this.nome = nome;
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Olá, " + nome
                + ", obrigado por criar uma conta em nosso banco, "
                + "sua agência é " + agencia
                + ", conta " + numero + " e seu saldo R$ " + String.format("%.2f", saldo)
                + " já está disponível para saque";
    }
}
